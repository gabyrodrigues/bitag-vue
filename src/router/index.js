import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home'
import Tags from '../views/Tags'
import Game from '../views/Game'
import GamesComparison from '../views/GamesComparison'
import Register from '../views/Register'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/tags',
    name: 'Tags',
    component: Tags
  },
  {
    path: '/game',
    name: 'Game',
    component: Game
  },
  {
    path: '/gamescomparison',
    name: 'GamesComparison',
    component: GamesComparison
  },
  {
    path: '/register',
    name: 'Register',
    component: Register
  }
]

const router = new VueRouter({
  routes
})

export default router
